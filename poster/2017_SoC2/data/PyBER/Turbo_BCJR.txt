Run command:
/panfs/panasas/cnt0026/ims1703/acassagn/aff3ct/build/./bin/aff3ct --term-freq "60000" -p "32" --sim-type "BFER" --cde-type "TURBO" -K "6144" -N "18432" -e "500" -m "0.0" -M "3.01" --dec-type "LTE" --dec-implem "VERY_FAST" --dec-simd "INTER" -i "6" --itl-type "LTE" --dec-sf "LTE_VEC" --dec-max "MAX" -e "500" --sim-pyber "LTE" 
Curve name:
Turbo BCJR-Max (6144,18432)
# -------------------------------------------------
# ---- A FAST FORWARD ERROR CORRECTION TOOL >> ----
# -------------------------------------------------
# Parameters:
# * Simulation ------------------------------------
#    ** Type                   = BFER
#    ** SNR min (m)            = 0.000000 dB
#    ** SNR max (M)            = 3.010000 dB
#    ** SNR step (s)           = 0.100000 dB
#    ** Type of bits           = int (32 bits)
#    ** Type of reals          = float (32 bits)
#    ** Inter frame level      = 8
#    ** Seed                   = 0
#    ** MPI comm. freq. (ms)   = 1000
#    ** MPI size               = 30
#    ** Multi-threading (t)    = 48 thread(s)
# * Code ------------------------------------------
#    ** Type                   = TURBO
#    ** Info. bits (K)         = 6144
#    ** Codeword size (N)      = 18432 + 12 (tail bits)
#    ** Coset approach (c)     = off
# * Source ----------------------------------------
#    ** Type                   = RAND
# * Encoder ---------------------------------------
#    ** Type                   = TURBO
#    ** Systematic encoding    = on
#    ** Polynomials            = {013,015}
#    ** Buffered               = on
# * Interleaver -----------------------------------
#    ** Type                   = LTE
# * Modulator -------------------------------------
#    ** Type                   = BPSK
#    ** Bits per symbol        = 1
#    ** Sampling factor        = 1
# * Channel ---------------------------------------
#    ** Type                   = AWGN
#    ** Domain                 = LLR
# * Demodulator -----------------------------------
#    ** Sigma square           = on
#    ** Max type               = unused
# * Decoder ---------------------------------------
#    ** Type (D)               = BCJR
#    ** Implementation         = VERY_FAST
#    ** SIMD strategy          = INTER
#    ** Num. of iterations (i) = 6
#    ** Scaling factor         = LTE_VEC
#    ** Max type               = MAX
# * Monitor ---------------------------------------
#    ** Frame error count (e)  = 500
#    ** Bad frames tracking    = off
#    ** Bad frames replay      = off
#
# The simulation is running...
# ----------------------------------------------------------------------||---------------------
#       Bit Error Rate (BER) and Frame Error Rate (FER) depending       ||  Global throughput  
#                    on the Signal Noise Ratio (SNR)                    ||  and elapsed time   
# ----------------------------------------------------------------------||---------------------
# -------|-------|----------|----------|----------|----------|----------||----------|----------
#  Es/N0 | Eb/N0 |      FRA |       BE |       FE |      BER |      FER || SIM_CTHR |    ET/RT 
#   (dB) |  (dB) |          |          |          |          |          ||   (Mb/s) | (hhmmss) 
# -------|-------|----------|----------|----------|----------|----------||----------|----------
   -4.77 |  0.00 |   282440 | 2.16e+08 |   282454 | 1.25e-01 | 1.00e+00 ||  3756.12 | 00h00'01  
   -4.67 |  0.10 |   320536 | 1.87e+08 |   320298 | 9.48e-02 | 9.99e-01 ||  5370.94 | 00h00'01  
   -4.57 |  0.20 |   320264 | 1.17e+08 |   315426 | 5.94e-02 | 9.85e-01 ||  5323.99 | 00h00'01  
   -4.47 |  0.30 |   320928 | 53235056 |   283656 | 2.70e-02 | 8.84e-01 ||  5568.77 | 00h00'01  
   -4.37 |  0.40 |   320880 | 15117896 |   188701 | 7.67e-03 | 5.88e-01 ||  5441.77 | 00h00'01  
   -4.27 |  0.50 |   318216 |  2449160 |    72868 | 1.25e-03 | 2.29e-01 ||  5282.49 | 00h00'01  
   -4.17 |  0.60 |   315616 |   207347 |    14575 | 1.07e-04 | 4.62e-02 ||  5245.60 | 00h00'01  
   -4.07 |  0.70 |   319712 |    10344 |     1684 | 5.27e-06 | 5.27e-03 ||  5328.26 | 00h00'01  
   -3.97 |  0.80 |  1009648 |     1468 |      538 | 2.37e-07 | 5.33e-04 ||  5730.99 | 00h00'03  
   -3.87 |  0.90 |  4016640 |     1218 |      541 | 4.94e-08 | 1.35e-04 ||  5762.98 | 00h00'12  
   -3.77 |  1.00 |  9747440 |     1116 |      503 | 1.86e-08 | 5.16e-05 ||  5776.87 | 00h00'31
   -3.67 |  1.10 | 19856664 |     1115 |      503 | 9.14e-09 | 2.53e-05 ||  5833.69 | 00h01'02  
   -3.57 |  1.20 | 38512160 |     1135 |      501 | 4.80e-09 | 1.30e-05 ||  5841.57 | 00h02'01
   -3.47 |  1.30 | 78560648 |     1114 |      502 | 2.31e-09 | 6.39e-06 ||  5887.40 | 00h04'05
   -3.37 |  1.40 | 1.32e+08 |     1103 |      502 | 1.37e-09 | 3.82e-06 ||  5917.81 | 00h06'49